<?php
require_once dirname(__FILE__).'/config.php';

ini_set('session.bug_compat_warn', 0);
ini_set('session.bug_compat_42', 0);
date_default_timezone_set ("Europe/London");

@session_start();

//Ajax Files
$_SESSION['ROOT_HTTP_SERVER'] = ROOT_HTTP_SERVER;

//$db = new Database();
?>