<?php
class EligibleControler extends Control
{
    private $Viewer;
    private $Modeler;

    function __construct()
    {
        $this->Model = 'EligibleModel';
        $this->View = 'EligibleView';
        $this->Viewer = $this->View();
        $this->Modeler = $this->Model();
    }

    function __get($name)
    {
        if(isset($this->$name))
            return $this->$name;
        else
            return false;
    }

    function index()
    {
        $this->Viewer->template();
    }
}
?>