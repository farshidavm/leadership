<?php
class EssenceControler extends Control
{
    private $Viewer;
    private $Modeler;

    function __construct()
    {
        $this->Model = 'EssenceModel';
        $this->View = 'EssenceView';
        $this->Viewer = $this->View();
        $this->Modeler = $this->Model();
    }

    function __get($name)
    {
        if(isset($this->$name))
            return $this->$name;
        else
            return false;
    }

    function index()
    {
        $this->Viewer->template();
    }
}
?>