<?php
class EventsControler extends Control
{
    private $Viewer;
    private $Modeler;

    function __construct()
    {
        $this->Model = 'EventsModel';
        $this->View = 'EventsView';
        $this->Viewer = $this->View();
        $this->Modeler = $this->Model();
    }

    function __get($name)
    {
        if(isset($this->$name))
            return $this->$name;
        else
            return false;
    }

    function index()
    {
        if(isset($_REQUEST['leadershipsubmit'])):
            submit_leadership_form();
        endif;

        $this->Viewer->template();
    }
}
?>