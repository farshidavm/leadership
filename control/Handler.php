<?php
require_once '../top.php';

if(!isset($_GET['Controler']) || empty($_GET['Controler']))
    $_GET['Controler'] = "index";

if(!file_exists(CONTROL_ADDRESS.strval(ucfirst(strtolower($_GET['Controler'])))."Controler.php"))
    header("Location: ".ROOT_HTTP_SERVER);

require_once CONTROL_ADDRESS.strval(ucfirst(strtolower($_GET['Controler'])))."Controler.php";
$class = ucfirst(strtolower($_GET['Controler']))."Controler";
$PageControler = new $class;
$PageControler->index();

require_once '../down.php';
?>