<?php
class Control
{
    protected $View;
    protected $Model;

    public function __construct() {}

    public function __destruct() {}

    protected function Model()
    {
        if(!file_exists(MODEL_ADDRESS.strval($this->Model).".php"))
            return false;

        require_once MODEL_ADDRESS.$this->Model.".php";
        $Model = new $this->Model;
        return $Model;
    }

    protected function View()
    {
        if(!file_exists(VIEW_ADDRESS.strval($this->View).".php"))
            return false;

        require_once VIEW_ADDRESS.$this->View.".php";
        $View = new $this->View;
        return $View;
    }
}
?>