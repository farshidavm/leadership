<?php
class ContactsControler extends Control
{
    private $Viewer;
    private $Modeler;

    function __construct()
    {
        $this->Model = 'ContactsModel';
        $this->View = 'ContactsView';
        $this->Viewer = $this->View();
        $this->Modeler = $this->Model();
    }

    function __get($name)
    {
        if(isset($this->$name))
            return $this->$name;
        else
            return false;
    }

    function index()
    {
        if(isset($_REQUEST['contactssubmit'])):
            submit_contacts_form();
        endif;
        $this->Viewer->template();
    }
}
?>