<?php
    require_once "../top.php";
    require_once MEMBERS_INCLUDE_ADDRESS."Members.php";

	$page = new Page();
	$page->setTitle("Global Academic Executive : Members");

	if (isset($basepath))
		$page->setBase($basepath);

	$page->linkCSS(CSS_FOLDER."main.css");
	$page->linkCSS(CSS_FOLDER."960_24_col.css");
	$page->linkJS(JS_FOLDER."members.js");
	$page->linkFavicon(IMAGE_FOLDER."Favicon.png", "image/png");

	$page->displayHead();

	$page->displayHeader();

	echo "<div id='Content'>";
        echo "<div class='header container_24'>";
            echo "<div class='grid_24'>";
                echo "<img src='".MEMBERS_IMAGE_FOLDER."members_heading.png'>";
            echo "</div>";
	    echo "</div>";

        echo "<div class='down container_24'>";
            echo "<div class='division grid_24'>&nbsp;</div>";
            $page->displaySidebarLeft();
            echo "<div class='maincontent grid_155'>";
                echo "<div class='plaintext'>";
                    foreach($Members as $key => $Member):
                    echo "<div class='".($key == 0 ? "" : "inner ")."grid_155 member'>";
                        echo "<div class='grid_25 avatar'><img src='".AVATAR_MEMBERS_IMAGE_FOLDER.$Member['avatar']."'; /></div>";
                        echo "<div class='grid_520 detail'>";
                            echo "<h1>".$Member['name']." ".$Member['surname']."</h1>";
                            echo "<h4>".$Member['position']."</h4>";
                            echo "<div class='description'>";
                                echo "<div class='bio grid_405'><span class='span_".$key."'>".$Member['description']['summary']."</span></div>";
                                echo "<div class='readmore grid_26'><img data-key='summary' id='".$key."' class='readmorebutton' src='".MEMBERS_IMAGE_FOLDER."readMore_btn.png'; /></div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                    endforeach;
                echo "</div>";
            echo "</div>";
        echo "</div>";

	echo "</div>";

	$page->displayFooter();

    require_once "../down.php";

?>