<?php
    require_once "../top.php";

	$page = new Page();
	$page->setTitle("Global Academic Executive : Purpose");

	if (isset($basepath))
		$page->setBase($basepath);

	$page->linkCSS(CSS_FOLDER."main.css");
	$page->linkCSS(CSS_FOLDER."960_24_col.css");
	$page->linkFavicon(IMAGE_FOLDER."Favicon.png", "image/png");

	$page->displayHead();

	$page->displayHeader();

	echo "<div id='Content'>";
        echo "<div class='header container_24'>";
            echo "<div class='grid_24'>";
                echo "<img src='".PURPOSE_IMAGE_FOLDER."purpose_heading.png'>";
            echo "</div>";
	    echo "</div>";

        echo "<div class='down container_24'>";
            echo "<div class='division grid_24'>&nbsp;</div>";
            $page->displaySidebarLeft();
            echo "<div class='maincontent grid_155'>";
                echo "<div class='plaintext'>";
                    echo "<img class='pagecontent' src='".PURPOSE_IMAGE_FOLDER."purpose_text.png'>";
                    echo "<img class='executivimage' src='".IMAGE_FOLDER."gaExecutive_text.png'>";
                echo "</div>";
            echo "</div>";
        echo "</div>";

	echo "</div>";

	$page->displayFooter();

    require_once "../down.php";

?>