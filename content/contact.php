<?php
    require_once "../top.php";

	$page = new Page();
	$page->setTitle("Global Academic Executive : Contact");

	if (isset($basepath))
		$page->setBase($basepath);

	$page->linkCSS(CSS_FOLDER."main.css");
	$page->linkCSS(CSS_FOLDER."960_24_col.css");
	$page->linkJSFile(JS_FOLDER."jquery.toastmessage.js");
	$page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
	$page->linkFavicon(IMAGE_FOLDER."Favicon.png", "image/png");

	$page->displayHead();

	$page->displayHeader();

	echo "<div id='Content'>";
        echo "<div class='header container_24'>";
            echo "<div class='grid_24'>";
                echo "<img src='".CONTACT_IMAGE_FOLDER."contact_heading.png'>";
            echo "</div>";
	    echo "</div>";

        echo "<div class='down container_24'>";
            echo "<div class='division grid_24'>&nbsp;</div>";
            $page->displaySidebarLeft();
            echo "<div class='maincontent grid_115'>";
                echo "<div class='contributioncontent'>";
                    echo "<img src='".CONTACT_IMAGE_FOLDER."sendMessage_heading.png'>";
                    contributionform();
                echo "</div>";
            echo "</div>";
            $page->displaySidebarRight();
        echo "</div>";

	echo "</div>";

	$page->displayFooter();

    require_once "../down.php";

?>