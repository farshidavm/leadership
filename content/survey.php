<?php
    require_once "../top.php";

	$page = new Page();
	$page->setTitle("Global Academic Executive : Survey");

	if (isset($basepath))
		$page->setBase($basepath);

	$page->linkCSS(CSS_FOLDER."main.css");
	$page->linkCSS(CSS_FOLDER."960_24_col.css");

	//$page->linkJSFile(JS_FOLDER."jquery.jalert.packed.js");
	$page->linkJSFile(JS_FOLDER."jquery.toastmessage.js");
	$page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
	$page->linkFavicon(IMAGE_FOLDER."Favicon.png", "image/png");

	$page->displayHead();

	$page->displayHeader();

	echo "<div id='Content'>";
        echo "<div class='header container_24'>";
            echo "<div class='grid_24'>";
                echo "<img src='".SURVEY_IMAGE_FOLDER."survey_heading.png'>";
            echo "</div>";
	    echo "</div>";

        echo "<div class='down container_24'>";
            echo "<div class='division grid_24'>&nbsp;</div>";
            $page->displaySidebarLeft();
            echo "<div class='maincontent grid_155'>";
                echo "<div class='plaintext'>";
                    echo "<div class='grid_155'>";
                        echo "<img class='pagecontent' src='".SURVEY_IMAGE_FOLDER."survey_image.jpg'>";
                    echo "</div>";
                    echo "<div class='grid_155'>";
                        echo "<div class='grid_55'>";
                                echo "<img class='pagecontent' src='".SURVEY_IMAGE_FOLDER."surveyS_heading.png'>";
                                echo "<img class='pagecontent' src='".SURVEY_IMAGE_FOLDER."survey_text.png'>";
                                echo "<a target='_blank' href='http://survey.gaexec.com/index.php/259452/lang-en' class='survey_btn_start'><img class='pagecontent' src='".SURVEY_IMAGE_FOLDER."survey_btn_start.png'></a>";
                        echo "</div>";

                        echo "<div class='grid_95 columnspace'>";
                                echo "<img class='pagecontent' src='".SURVEY_IMAGE_FOLDER."contribute_heading.png'>";
                                echo "<img class='pagecontent' src='".SURVEY_IMAGE_FOLDER."contribute_text.png'>";
                                contributionform();
                        echo "</div>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";
        echo "</div>";

	echo "</div>";

	$page->displayFooter();

    require_once "../down.php";

?>