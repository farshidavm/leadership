<?php
class PurposeView extends View
{
    public function __construct()
    {
        parent::__construct();

	    //$this->page->linkJSFile(JS_FOLDER."members.js");
	    //$this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function getBox()
    {
        $this->page->Boxes->pageBoxLeft("purpose");
    }

    public function display()
    {
    	echo "<div id='Content' class='purposepage actualpage'>";
            $this->page->displaySubHeader("Purpose");

            echo "<div class='top container_24'>";
                echo "<div class='left grid_custom'>";
                    echo "<div class='boxes'>";
                        $this->getBox();
                    echo "</div>";
                echo "</div>";

                echo "<div class='right grid_custom'>";
                    echo "<div class='marginbottom'><img src='".PURPOSE_IMAGE_FOLDER."programScope.png' alt='' title='' /></div>";
                    echo "<div class='margin3bottom'><img src='".PURPOSE_IMAGE_FOLDER."programGoals.png' alt='' title='' /></div>";
                    echo "<div class='marginbottom'><img src='".PURPOSE_IMAGE_FOLDER."programOverview.png' alt='' title='' /></div>";
                echo "</div>";
    	    echo "</div>";

    	echo "</div>";
    }
}
?>