<?php
class EssenceView extends View
{
    public function __construct()
    {
        parent::__construct();

	    //$this->page->linkJSFile(JS_FOLDER."members.js");
	    //$this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function getBox()
    {
        $this->page->Boxes->pageBoxLeft("essence");
    }

    public function display()
    {
    	echo "<div id='Content' class='essencepage actualpage'>";
            $this->page->displaySubHeader("Essence of leadership", "Essence of leadership", "Essence of leadership");
            echo "<div class='top container_24 textbased'>";
                echo "<div class='grid_custom'><img src='".IMAGE_FOLDER."john.jpg' /></div>";
                echo "<div class='grid_custom texttoimage'>";
                    echo "<h1 class='page-header margin2bottom'>John Bertrand on the essence of leadership</h1>";
                    echo "<p class='page-content margin2bottom'>Great leaders are both dream builders and team builders.</p>";
                    echo "<p class='page-content margin2bottom'>They create a vision, they communicate that vision, they constantly re-inforce that vision. They inspire, they do.</p>";
                    echo "<p class='page-content margin2bottom'>Leaders create winning teams around their vision. They encourage, empower and develop their people. They demand innovation, creativity and boldness.</p>";
                    echo "<p class='page-content margin2bottom'>Leaders provide purpose, strategy, direction, and alignment. They know what success will look like. They anticipate change, and when it happens, they adapt.</p>";
                    echo "<p class='page-content margin2bottom'>They have high emotional intelligence, they trust their intuition.</p>";
                    echo "<p class='page-content margin2bottom'>They celebrate their team's milestones, their successes.</p>";
                    echo "<p class='page-content margin2bottom'>They operate within a very strong set of values of trust, integrity, honesty, transparency, humility. The culture within these teams is extremely strong. People want to be involved.</p>";
                    echo "<p class='page-content margin2bottom'>They love what they do, they're passionate people.</p>";
                    echo "<p class='page-content margin2bottom'>And leaders never give up.</p>";
                echo "</div>";
    	    echo "</div>";
    	echo "</div>";
    }
}
?>