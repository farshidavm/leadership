<?php
class EventsView extends View
{
    public function __construct()
    {
        parent::__construct();

	    $this->page->linkJSFile(JS_FOLDER."function.js");
	    $this->page->linkJSFile(JS_FOLDER."jquery.toastmessage.js");
	    $this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function display()
    {
    	echo "<div id='Content' class='eventspage actualpage'>";
            $this->page->displaySubHeader("Public Events", "Public Events", "Public Events");
            echo "<div class='top container_24 textbased'>";
                echo "<h1 class='page-header'>Public Events</h1>";

                echo "<p class='page-content'>The public are welcome to attend the following events, the leadership conversations John Bertrand.  An on-line registration form will be made available soon. In the meantime, expressions of interest may be forwarded to <a href='mailto:jodie.roker@monash.edu' class='inline-link'>jodie.roker@monash.edu</a>.</p>";

                echo "<p class='page-content marginbottom'>17 July 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Paul Roos</a>, Former Premiership Coach Sydney Swans and 2005 Australian Coach of the Year</p>";
                echo "<p class='page-content'>Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>24 July 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Major General Angus Campbell, DSC, AM</a>, Deputy Chief of Army</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>29 July 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Professor Christine Kilpatrick</a>, CEO of The Royal Children&#39;s Hospital Melbourne</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>9 October 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Alan Joyce</a>, CEO of Qantas</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>16 October 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>The Hon RJL (Bob) Hawke AC</a>, Prime Minister of Australia 1983-1991</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>23 October 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Dr Fiona Wood, AM</a>, Pioneering burns specialist and Australian of the Year 2005</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";
            echo "</div>";
    	echo "</div>";
    }
}
?>