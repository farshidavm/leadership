<?php
class IndexView extends View
{
    public function __construct()
    {
        parent::__construct();
    }

    public function display()
    {
        global $Conversations, $Members;
    	echo "<div id='Content' class='homepage actualpage'>";
            echo "<div class='top'>";
                echo "<div class='banner container_24'>";
                    echo "<img alt='John Bertrand Leadership Series' title='John Bertrand Leadership Series' src='".IMAGE_FOLDER."main_text.png'>";
                    echo "<span alt='Leaders create a vision. They communicate that vision. They inspire. They do.' title='John Bertrand Leadership Series' class='quote'>";
                        echo "<h1>";
                            echo "<span class='startquote doublequote'>&#8220;</span>";
                            echo "<div class='textquote'>Leaders create a vision. They communicate that vision. They inspire. They do.</div>";
                            echo "<span class='endquote doublequote'>&#8221;</span>";
                            echo "<div class='ownerquote'>&#8212; John Bertrand</div>";
                        echo "</h1>";
                    echo "</span>";
                echo "</div>";
    	    echo "</div>";

            /*
            echo "<div class='middle memberslider container_24'>";
                Boxes::members_slider($Members);
            echo "</div>";
            */

            echo "<div class='down container_24'>";
                echo "<div id='johninfo' class='grid_custom'>";
                    echo "<div class='left grid_custom'>";
                        echo "<div class='grid_custom'><img style='vertical-align: top; margin: 0 15px 0 0;' alt='' title='' src='".IMAGE_FOLDER."john_image.jpg' /></div>";

                        echo "<div class='gird_custom'>";
                            echo "<h1 class='page-header nomarginbottom'>Monash University John Bertrand Leadership Series</h1>";
                            echo "<p class='page-content'>Leadership can be learnt. So why not learn from the best?</p>";
                            echo "<p class='page-content'>The John Bertrand Leadership Series is an invitation-only program that gives you the chance to meet brilliant Australian leaders and learn the secrets of their success. Our leaders have achieved great things, both individually and within high-performance teams, across a diverse range of industries. They have led sporting teams and companies to victory. They have run hospitals, and they have run the country.</p>";
                            echo "<p class='page-content'>You will be guided toward leadership excellence by John Bertrand AM - international businessman, philanthropist, and, of course, skipper of Australia 2 and winner of the 1983 America&#39;s Cup. This victory was considered by the Confederation of Australian Sport as the greatest team performance in the last 200 years of Australian sport.</p>";
                            echo "<p class='page-content'>Beginning in July 2013, this four-month program also involves a dynamic mix of discussions, challenging simulations, intense group work, and networking opportunities. It demands intellectual rigour, solitary reflection, and commitment.</p>";
                        echo "</div>";
                    echo "</div>";

                    echo "<div class='right grid_custom'>";
                        echo "<div class='textboxed' style='padding: 0 0 10px 0;'>";
                            echo "<p class='page-content marginbottom' style='margin-bottom: 10px !important; font-size: 17px;'><b class='bolder'>Our program will</b>:</p>";
                            echo "<ul class='inline-list'>";
                                echo "<li class='marginbottom'><b class='bolder'>Foster</b> your personal leadership values</li>";
                                echo "<li class='marginbottom'><b class='bolder'>Inspire</b> you to achieve your leadership vision</li>";
                                echo "<li class='marginbottom'><b class='bolder'>Strengthen</b> your leadership abilities</li>";
                                echo "<li class='marginbottom'><b class='bolder'>Enhance</b> your ability to create positive change</li>";
                                echo "<li class='marginbottom'><b class='bolder'>Provide</b> you with an opportunity to work with other passionate and committed individuals</li>";
                                echo "<li class='marginbottom'><b class='bolder'>Strengthen</b> your learning with practical experience from exceptional past and present leaders.</li>";
                            echo "</ul>";
                        echo "</div>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";

            echo "<div class='bottom container_24'>";
                echo "<h1>John Bertand meets:</h1>";
                foreach($Conversations as $key => $Conversation):
                    $class = "conversation grid_custom";
                    if($key % 2 == 0)
                        $class .= " even";
                    else
                        $class .= " odd";

                    echo "<div class='".$class."'>";
                        echo "<a alt='Leadership Speakers - ".$Conversation['alt']."' title='Leadership Speakers - ".$Conversation['alt']."' href='".ROOT_HTTP_SERVER."speakers'><img alt='' title='' src='".CONVERSATION_IMAGE_FOLDER.$Conversation['image']."' /></a>";
                    echo "</div>";
                endforeach;
            echo "</div>";

    	echo "</div>";
    }
}
?>