<?php
class SeriesView extends View
{
    public function __construct()
    {
        parent::__construct();

	    //$this->page->linkJSFile(JS_FOLDER."members.js");
	    //$this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function display()
    {
    	echo "<div id='Content' class='seriespage actualpage'>";
            $this->page->displaySubHeader("Leadership series", "Leadership series", "Leadership series");
            echo "<div class='top container_24 textbased'>";
                echo "<h1 class='page-header'>Leadership series</h1>";
                echo "<p class='page-content'>A great leader makes things happen. Leaders have an enhanced ability to create, motivate, organise, influence, and inspire. Leaders change things for the better, whether it&#39;s in their own community or around the world.</p>";
                echo "<p class='page-content'>The John Bertrand Leadership Series combines academic leadership principles, hands-on learning, and the opportunity to pick the brains of great leaders in politics, the military, sport, business, health and science.</p>";
                echo "<p class='page-content'>The program begins with a foundation day hosted by academic leaders from the <a href='http://www.mba.monash.edu.au/' target='_blank' class='inline-link'>Monash University MBA program</a>. This intense day uses speakers, simulations, discussion and workshops to sharpen your skills and challenge your thinking.</p>";
                echo "<p class='page-content'>During the speaker series, you&#39;ll spend six engaging evenings listening to and questioning some of Australia&#39;s most inspiring leaders. John Bertrand AM &#8211; himself one of the nation&#39;s pre-eminent leaders &#8211; will probe each speaker&#39;s leadership journey, failures, and successes, and explore the resilience it takes to deliver on a vision.</p>";
                echo "<p class='page-content'>The two-part course is held in July and October, and requires a commitment lasting over four months.</p>";

                /*
                echo "<h2 class='page-sub-header marginbottom'>Program Scope</h2>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'><b class='bolder'>Monash University&#39;s John Bertrand Leadership Series</b> is a unique program for Monash University students and alumni who have demonstrated outstanding leadership qualities and, as emerging leaders, are the best amongst their chosen field.</li>";
                    echo "<li class='marginbottom'>Participants are under 40 years in age.</li>";
                    echo "<li class='marginbottom'>Program launch is July 2013.</li>";
                    echo "<li class='marginbottom'>Program duration is 4 months</li>";
                    echo "<li class='marginbottom'>Program consists of the following:</li>";
                        echo "<ul class='inline-list'>";
                            echo "<li class='marginbottom'>Foundation Day</li>";
                            echo "<li class='marginbottom'>6 Prestigious Evening Speaker Series</li>";
                            echo "<li class='marginbottom'>Private Q&A session for participants</li>";
                            echo "<li class='marginbottom'>Masterclass series</li>";
                        echo "</ul>";
                echo "</ul>";

                echo "<h2 class='page-sub-header marginbottom'>Program Goals</h2>";
                echo "<p class='page-content'><b class='bolder'>Monash University&#39;s John Bertrand Leadership Series</b> offers a range of opportunities and activities that will:</p>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'><b class='bolder'>Monash University&#39;s John Bertrand Leadership Series</b> is a unique program for Monash University students and alumni who have demonstrated outstanding leadership qualities and, as emerging leaders, are the best amongst their chosen field.</li>";
                    echo "<li class='marginbottom'><b class='bolder'>foster</b> your sense of personal leadership values</li>";
                    echo "<li class='marginbottom'><b class='bolder'>inspire</b> you to greatness</li>";
                    echo "<li class='marginbottom'><b class='bolder'>affirm</b> and strengthen your existing leadership abilities</li>";
                    echo "<li class='marginbottom'><b class='bolder'>enhance</b> your ability to effect positive change</li>";
                    echo "<li class='marginbottom'><b class='bolder'>provide</b> you with an opportunity to work with other passionate and committed individuals</li>";
                    echo "<li class='marginbottom'><b class='bolder'>facilitate</b> your learning from exceptional past and present leaders.</li>";
                echo "</ul>";
                */

                echo "<h2 class='page-sub-header'>Who is eligible?</h2>";
                echo "<p class='page-content'>Our participants are Monash University students and alumni who have demonstrated outstanding leadership qualities and are among the best in their chosen field. There will be 40 participants, all under 40 years old.</p>";
                echo "<p class='page-content'>You cannot apply for this program. The deans of Monash faculties nominate current or former students based on their accomplishments, attributes and experiences. We&#39;re looking for the best, which is why the program is fully funded.</p>";

                echo "<h1 class='quote'>";
                    echo "<div class='textquote'><span class='doublequote'>&#8220;</span><span>...equip yourself for life, not solely for your own benefit but for the benefit of the whole community.</span><span class='endquote doublequote'>&#8221;</span></div>";
                    echo "<div class='ownerquote'>&#8212; Sir John Monash</div>";
                    echo "<div class='clear'></div>";
                echo "</h1>";
    	    echo "</div>";
    	echo "</div>";
    }
}
?>