<?php
class SpeakersView extends View
{
    public function __construct()
    {
        parent::__construct();

	    $this->page->linkJSFile(JS_FOLDER."members.js");
	    //$this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function display()
    {
    	echo "<div id='Content' class='speakerpage actualpage'>";
            $this->page->displaySubHeader("Speakers");

            echo "<div class='top container_24 textbased'>";
                echo "<div class='plaintext'>";
                    foreach($GLOBALS['Members'] as $key => $Member):
                    echo "<div class='".($key == 0 ? "" : "inner ")."grid_custom member'>";
                        echo "<div class='grid_25 avatar'><img src='".AVATAR_MEMBERS_IMAGE_FOLDER.$Member['avatar']."'; /></div>";
                        echo "<div class='grid_custom detail'>";
                            echo "<h1>".$Member['name']." ".$Member['surname']."</h1>";
                            echo "<h4>".$Member['profession']."</h4>";
                            echo "<div class='description'>";
                                echo "<div class='bio grid_custom'><span class='span_".$key."'>".$Member['summary']."</span></div>";
                                echo "<div class='readmore grid_26'><img data-key='summary' id='".$key."' class='readmorebutton' src='".SPEAKERS_IMAGE_FOLDER."readMore_btn.png'; /></div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                    endforeach;
                echo "</div>";
    	    echo "</div>";

    	echo "</div>";
    }
}
?>