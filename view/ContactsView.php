<?php
class ContactsView extends View
{
    public function __construct()
    {
        parent::__construct();

	    $this->page->linkJSFile(JS_FOLDER."jquery.toastmessage.js");
	    $this->page->linkJSFile(JS_FOLDER."function.js");
	    $this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function display()
    {
    	echo "<div id='Content' class='contactspage actualpage'>";
            $this->page->displaySubHeader("Contact us", "Contact us", "Contact us");
            echo "<div class='top container_24 textbased'>";
                echo "<h1 class='page-header'>Contacts</h1>";
                echo "<p class='page-content'><b class='bolder'>The John Bertrand Leadership Series is coordinated by Monash HR, Monash University.</b></p>";
                echo "<p class='page-content'>Please direct all enquiries to Jodie Roker at <a class='inline-link' href='mailto:jodie.roker@monash.edu'>jodie.roker@monash.edu</a></p>";
                //contacts_form();
    	    echo "</div>";
    	echo "</div>";
    }
}
?>