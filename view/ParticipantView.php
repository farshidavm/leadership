<?php
class ParticipantView extends View
{
    public function __construct()
    {
        parent::__construct();

	    $this->page->linkJSFile(JS_FOLDER."members.js");
	    //$this->page->linkCSS(CSS_FOLDER."jquery.toastmessage.css");
    }

    public function display()
    {
    	echo "<div id='Content' class='participantpage actualpage'>";
            $this->page->displaySubHeader("Participant program", "Participant program", "Participant program");

            echo "<div class='top container_24 textbased'>";
                echo "<h1 class='page-header marginbottom'>Participant program</h1>";

                echo "<h2 class='page-sub-header marginbottom'>Program dates</h2>";

                echo "<p class='page-content marginbottom'>2 July 2013 - Foundation day (8:00am - 6:00pm lunch, morning and afternoon tea provided)</p>";
                echo "<p class='page-content'>MBA Office, Monash University, Level 4, Building N, Monash University, Caulfield</p>";

                echo "<p class='page-content marginbottom'>17 July 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Paul Roos</a>, Former Premiership Coach Sydney Swans and 2005 Australian Coach of the Year</p>";
                echo "<p class='page-content'>Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>24 July 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Major General Angus Campbell, DSC, AM</a>, Deputy Chief of Army</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>29 July 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Professor Christine Kilpatrick</a>, CEO of The Royal Children&#39;s Hospital Melbourne</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>9 October 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Alan Joyce</a>, CEO of Qantas</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>16 October 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>The Hon RJL (Bob) Hawke AC</a>, Prime Minister of Australia 1983-1991</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content marginbottom'>23 October 2013 - <a class='inline-link' href='".ROOT_HTTP_SERVER."speakers'>Dr Fiona Wood, AM</a>, Pioneering burns specialist and Australian of the Year 2005</p>";
                echo "<p class='page-content'>Clemenger Auditorium, National Gallery of Victoria, Melbourne (6:00-9:00pm)</p>";

                echo "<p class='page-content'>25 November 2013 - Presentation evening, Monash University, Caulfield Campus (4:00-6:00pm)</p>";

                echo "<h2 class='page-sub-header marginbottom'>Program outline</h2>";
                echo "<h3 class='page-sub-sub-header marginbottom'>Foundation day</h3>";
                echo "<p class='page-content marginbottom'>Foundation Day is hosted by senior academic leaders from the <a href='http://www.mba.monash.edu.au/' target='_blank' class='inline-link'>Monash University MBA program</a>. The day provides a framework for analysis for the upcoming speaker series, and sees a dynamic mix of guest speakers, intellectual rigour, challenging simulations, moderated discussion, group work and individual reflection.</p>";
                echo "<p class='page-content marginbottom'>You will explore:</p>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>authentic and personal leadership</li>";
                    echo "<li class='marginbottom'>challenges of leadership</li>";
                    echo "<li class='marginbottom'>theoretical, philosophical and conceptual foundations of leadership</li>";
                    echo "<li class='marginbottom'>values, ethics and emotional intelligence</li>";
                    echo "<li class='marginbottom'>assumptions about leadership</li>";
                    echo "<li class='marginbottom'>developing high performing teams.</li>";
                echo "</ul>";

                echo "<h3 class='page-sub-sub-header nomarginbottom'>The speaker series</h3>";
                echo "<p class='page-content'>This series sees John Bertrand AM in conversation with six of the nation&#39;s most outstanding leaders. Participants will have the opportunity to ask questions. And, after each event, participants will enjoy a rare networking opportunity with the speaker - and, of course, your talented peers. The Series will be broadcast by ABC television.</p>";

                echo "<h3 class='page-sub-sub-header nomarginbottom'>Presentation evening</h3>";
                echo "<p class='page-content'>This relaxed evening, hosted by John Bertrand AM and in the company of the Monash University Vice-Chancellor and President, Professor Ed Byrne AO, marks the end of the program. You will reflect on the program&#39;s impact on your individual leadership journey, solidify your goals and bond with your fellow participants. Participants who attended the Foundation Day and at least five of the six speaker evenings will be awarded Certificate of Participation from Monash University.</p>";

                echo "<h2 class='page-sub-header nomarginbottom'>Benefits</h2>";
                echo "<p class='page-content nomarginbottom'>Monash University&#39;s John Bertrand Leadership Series is an opportunity available only to Monash University students and alumni who have demonstrated outstanding leadership qualities, and are regarded as the best in their chosen field. We hope the benefits of this series will resonate throughout your working life.</p>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>Leadership development helps to unlock your potential. You&#39;ll learn to translate your talents and hard work into positive impacts on your peers, your career and the world.</li>";
                    echo "<li class='marginbottom'>The program combines academic principles in leadership with the testimony of real people who have used those principles to create change.</li>";
                    echo "<li class='marginbottom'>Participants will meet and learn from some of the world&#39;s most inspiring leaders, with backgrounds and operating contexts ranging from politics, the military, sport, business, health and science.</li>";
                    echo "<li class='marginbottom'>This program is fully funded. Our goal is to empower you. Participants selected for the program participate for free.</li>";
                echo "</ul>";

                /*
                echo "<h2 class='page-sub-header marginbottom'>Program Structure</h2>";
                echo "<h3 class='page-sub-sub-header marginbottom'>1- Foundation Day</h3>";
                echo "<p class='page-content marginbottom'>Some of the key themes to be examined in the Program include:</p>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>Authentic & Personal Leadership</li>";
                    echo "<li class='marginbottom'>Challenges of leadership- practical application</li>";
                    echo "<li class='marginbottom'>Theoretical, philosophical and conceptual foundations of leadership</li>";
                    echo "<li class='marginbottom'>Values, ethics and emotional intelligence</li>";
                    echo "<li class='marginbottom'>Assumptions about leadership</li>";
                    echo "<li class='marginbottom'>Developing High Performance Teams  &#8220;Winning the America&#39;s Cup&#8221;</li>";
                echo "</ul>";
                echo "<h3 class='page-sub-sub-header marginbottom'>2- Prestigious Speaker Series</h3>";
                echo "<h4 class='page-sub-sub-sub-header marginbottom'>Monash University has a conversation with 6 of the nations most outstanding leaders.</h4>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>A Q&A session hosted by John Bertrand AM,</li>";
                    echo "<li class='marginbottom'>Hosted at the National Gallery of Victoria.</li>";
                    echo "<li class='marginbottom'>3 forums in July and 3 in forums in October 2013.</li>";
                echo "</ul>";
                echo "<h4 class='page-sub-sub-sub-header marginbottom'>Distinguished Leaders</h4>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>Hosted by Mr John Bertrand AM.</li>";
                    echo "<li class='marginbottom'>Major General Angus Campbell DSC,AM.  Deputy Chief of Army.</li>";
                    echo "<li class='marginbottom'>Alan Joyce</li>";
                    echo "<li class='marginbottom'>Dr Christine Kilpatrick, CEO Royal Children&#39;s Hospital</li>";
                    echo "<li class='marginbottom'>Mr Paul Roos, Sydney Swans Academy, 2005 Aus Coach of the Year</li>";
                    echo "<li class='marginbottom'>The Hon. Robert Hawke</li>";
                    echo "<li class='marginbottom'>Dr Fiona Wood, Former Australian of the Year</li>";
                echo "</ul>";
                echo "<h3 class='page-sub-sub-header marginbottom'>3- Monash Masterclass Sessions</h3>";
                echo "<p class='page-content marginbottom'>Access is online or via a digital platform such as Google hangout.</p>";
                echo "<p class='page-content marginbottom'>Building on individual and personal leadership capabilities to lead people and implement strategic thinking.</p>";
                echo "<p class='page-content marginbottom'>Up close and personal with global leaders of Business, Science, Media, Law, Sport and Academia.</p>";
                echo "<p class='page-content marginbottom'>Mentoring discussions on managing people, industry issues, emerging markets and leadership challenges.</p>";
                echo "<p class='page-content marginbottom'>Participants and Guests have the opportunity to explore and share industry or issue specific ideas.</p>";
                echo "<p class='page-content marginbottom'>Proposed Guests:</p>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>John Bertrand</li>";
                    echo "<li class='marginbottom'>John Brumby</li>";
                    echo "<li class='marginbottom'>Paul Ramadge</li>";
                    echo "<li class='marginbottom'>Frieder Seible</li>";
                    echo "<li class='marginbottom'>Dr Sam Prince</li>";
                    echo "<li class='marginbottom'>Paul MacNamee</li>";
                echo "</ul>";

                echo "<h2 class='page-sub-header marginbottom'>Program overview</h2>";
                echo "<h3 class='page-sub-sub-header marginbottom'>&#8220;Leaders don&#39;t create followers, they create more leaders.&#8221; - Tom Peters</h3>";
                echo "<ul class='inline-list'>";
                    echo "<li class='marginbottom'>Leadership training helps to unlock your potential - enhancing your ability to impact your peers, your career and have a positive influence on the world.</li>";
                    echo "<li class='marginbottom'><b class='bolder'>Monash University&#39;s John Bertrand Leadership Series</b> is a highly prestigious leadership program and unique opportunity available to Monash University students and Alumni who have demonstrated outstanding leadership qualities, and, as emerging leaders, are regarded as the best in their chosen field.</li>";
                    echo "<li class='marginbottom'><b class='bolder'>Monash University&#39;s John Bertrand Leadership Series</b> will inspire exceptional leadership through real world, real-life learning. </li>";
                    echo "<li class='marginbottom'>On the subject of leadership there is often debate between the value of academic learning versus the benefits of &#8216;in field&#8217; training. The objective of this program is to apply a combination of academic principles in leadership followed by &#8216;real life&#8217; examples relating to it&#39;s application from those that have already achieved greatness.</li>";
                    echo "<li class='marginbottom'>Participants get to meet, converse with, and hear from some of the world&#39;s most inspiring leaders, with backgrounds ranging from politics, military, sport ,business, and science.</li>";
                    echo "<li class='marginbottom'>Students selected for the program participate in a fully-funded program.</li>";
                    echo "<li class='marginbottom'>The program consists of a Foundation Day involving all participants, senior university staff from Monash University Business & Economics MBA program together with key speakers drawn from across Australia.</li>";
                    echo "<li class='marginbottom'>Foundation Day is followed by six prestigious evening speaker forums held in the Clemenger Auditorium, National Gallery of Victoria.</li>";
                    echo "<li class='marginbottom'>Participants will explore the &#8216;essence of great leadership&#8217; with some of the nations finest leaders and tackle a range of contemporary leadership issues.</li>";
                    echo "<li class='marginbottom'>Hosted by one of the nations pre-eminent leaders, John Bertrand AM, the speaker series is conducted in a relaxed Q&A format. John Bertrand will probe and explore the individual&#39;s leadership journey, failures, success and the resilience it takes to achieve greatness. Post the interview, participants will enjoy networking opportunities with the nominated leader/speaker.</li>";
                    echo "<li class='marginbottom'>Participants will have the opportunity to debate, discuss, challenge, and consider a range of stimulating leadership topics via Monash Masterclass sessions. These aim to foster ongoing dialogue on leadership principles, styles and relevance in a global context.</li>";
                    echo "<li class='marginbottom'>As a member of this unique program, you will have the opportunity to engage with like-minded people from a diverse range of industries and Monash faculties. The program provides networking opportunities centered around mentoring and personal leadership growth.</li>";
                    echo "<li class='marginbottom'>In recognition of your participation in the program you will be awarded a certificate of participation from Monash University with access to ongoing leadership information and development.</li>";
                echo "</ul>";
                //*/
            echo "</div>";
    	echo "</div>";
    }
}
?>