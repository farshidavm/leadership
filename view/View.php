<?php
abstract class View
{
    public $page;

	abstract public function display();

    public function __construct()
    {
	    $this->page = new Page();
	    $this->page->setTitle("Monash Leadership");

	    if (isset($basepath))
		    $this->page->setBase($basepath);

	    $this->page->linkCSS(CSS_FOLDER."960_24_col.css");
	    $this->page->linkCSS(CSS_FOLDER."main.css");
	    $this->page->linkCSS(CSS_FOLDER."slider.css");
	    $this->page->linkCSS(CSS_FOLDER."view.css");
	    $this->page->linkCSS(CSS_FOLDER."browser.css");
	    $this->page->linkCSS(CSS_FOLDER."function.css");

	    $this->page->linkFavicon(IMAGE_FOLDER."Favicon.png", "image/png");
    }

	public function getBasepath()
    {
	    return $this->page->getBase();
	}

    public function template()
    {
    	$this->page->displayHead();
    	$this->page->displayHeader();

        $this->display();

        $this->page->displayFooter();
    }
}
?>