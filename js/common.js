$(document).ready(function()
{
    if(jQuery('#members_slider').length >0)
    {
        jQuery('#members_slider').jcarousel(
        {
        	wrap            : 'circular',
            visible         : 4,
            auto            : 5,
            scroll          : 1,
            buttonNextHTML  : null,
            buttonPrevHTML  : null
        });
    }

    $("#ticketsubmit").click(function() {
        $("#ticket").submit();
    });

    $(".input input").each(function () {
        $(this).val($(this).attr("datadefault"));
    });

    $(".input input").click(function() {
        placeHolder($(this).attr("id"), 'click');
    });

    $(".input input").blur(function() {
        placeHolder($(this).attr("id"), 'blur');
    });

    $("#Icon, #Menu, .subheader, .menu-tab").hover
    (
        function ()
        {
            $("div.menu-tab-submenu").addClass("hidden");
        },
        function () {}
    );


    $(".menubutton").hover
    (
        function ()
        {
            if($(this).hasClass("havesubmenu"))
            {
                var category = $(this).attr('dataparent');
                if(category != undefined)
                    $("div."+category).removeClass("hidden");
            }
            else
                $("div.menu-tab-submenu").addClass("hidden");
        },
        function () {}
    );

    $(".menu-tab-submenu").hover
    (
        function ()
        {},
        function ()
        {
            $(this).addClass("hidden");
        }
    );
});

function preloadimages(arr)
{
    var newimages=[]
    var arr=(typeof arr!="object")? [arr] : arr //force arr parameter to always be an array
    for (var i=0; i<arr.length; i++)
    {
        newimages[i]=new Image()
        newimages[i].src=arr[i]
    }
}

function imagehref(address)
{
    if(address != "")
    {
        document.location.href=address;
    }
    else
    {
        document.location.hostname;
    }
}

function validate_email_address(emailAddress)
{
	var pattern = new RegExp(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i);
	return pattern.test(emailAddress);
}

function placeHolder(inputid, action)
{
    if(action == undefined)
        action = 'click';

    if(inputid == undefined)
        inputid = 0;

    var input = inputid;

    if($("#"+inputid).length > 0)
    {
        if(action == 'click')
        {
            if($("#"+inputid).val() == $("#"+inputid).attr("datadefault"))
                $("#"+inputid).val("");
        }

        if(action == 'blur')
        {
            if($("#"+inputid).val() == "")
                $("#"+inputid).val($("#"+inputid).attr("datadefault"));

            if($("#"+inputid).val() == " ")
                $("#"+inputid).val($("#"+inputid).attr("datadefault"));
        }
    }
}