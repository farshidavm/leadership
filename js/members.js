$(document).ready(function()
{
    $("img.readmorebutton").click(function(event){
        var type = $(this).attr("data-key");
        var key = $(this).attr("id");
        $.ajax
    	({
    	    url		: "./ajax/Members/get_members_full_description.php",
    		data	:
    		{
    		    type    : $(this).attr("data-key"),
    			key     : $(this).attr("id")
    		},
    		type	: "post",
    		beforeSend: function(msg)
    		{},
    		success	: function(data)
    		{
    		    if(data != "failed")
                {
                    $(".span_"+key).html(data);

                    if($("img#"+key).attr("data-key") == "summary")
                    {
                        $("img#"+key).attr("data-key", "detailed");
                        $("img#"+key).attr("src", "./images/Speakers/showLess_btn.png");
                    }
                    else
                    {
                        $("img#"+key).attr("data-key", "summary");
                        $("img#"+key).attr("src", "./images/Speakers/readMore_btn.png");
                    }
                }
    		}
    	});
    });
});