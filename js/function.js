$(document).ready(function()
{
    $("form#leadership").submit(function()
    {
        if(!$("form#leadership #contactname").val() || $("form#leadership #contactname").val() == 'Contact Name')
        {
            $("form#leadership #contactname").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your contact name</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#leadership #position").val() || $("form#leadership #position").val() == 'Position')
        {
            $("form#leadership #position").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your position</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#leadership #employer").val() || $("form#leadership #employer").val() == 'Employer')
        {
            $("form#leadership #employer").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your employer</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#leadership #phone").val() || $("form#leadership #phone").val() == 'Phone')
        {
            $("form#leadership #phone").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your contact phone</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#leadership #email").val() || $("form#leadership #email").val() == 'Email')
        {
            $("form#leadership #email").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your email address</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!validate_email_address($("form#leadership #email").val()))
        {
            $("form#leadership #email").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your correct email address</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }
    });

    $("form#contacts").submit(function()
    {
        if(!$("form#contacts #name").val() || $("form#contacts #name").val() == 'Contact Name')
        {
            $("form#contacts #name").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your contact name</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#contacts #email").val() || $("form#contacts #email").val() == 'Email')
        {
            $("form#contacts #email").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your email address</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!validate_email_address($("form#contacts #email").val()))
        {
            $("form#contacts #email").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your correct email address</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#contacts #messagetitle").val() || $("form#contacts #messagetitle").val() == 'Title')
        {
            $("form#contacts #messagetitle").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your message title/span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }

        if(!$("form#contacts #message").val() || $("form#contacts #message").val() == 'Message')
        {
            $("form#contacts #message").focus();
            $().toastmessage('showToast',
            {
                text     : '<span style="color: #ffffff;"><br />Please enter your message</span>',
                sticky   : false,
                position : 'top-right',
                type     : 'error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed");
                }
            });
            return false;
        }
    });
});