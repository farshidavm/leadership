<?php
function curPageURL()
{
    $pageURL = 'http';
    if(isset($_SERVER["HTTPS"]))
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    }
    else
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function leadership_form()
{
?>
<div class="leadershipform grid_custom">
    <div class='form-title'>Register yourself today!</div>
    <form name="leadership" id="leadership" method="post">
        <div class="input"><input placeholder="Contact Name" name="contactname" id="contactname" type="text" required="required" datadefault='Contact Name' /></div>
        <div class="input"><input placeholder="Position" name="position" id="position" type="text" required="required" datadefault='Position' /></div>
        <div class="input"><input placeholder="Employer" name="employer" id="employer" type="text" required="required" datadefault='Employer' /></div>
        <div class="input"><input placeholder="Phone" name="phone" id="phone" type="text" required="required" datadefault='Phone' /></div>
        <div class="input"><input placeholder="Email" name="email" id="email" type="text" required="required" datadefault='Email' /></div>
        <div class="input"><input type="submit" name='leadershipsubmit' id='leadershipsubmit' /></div>
    </form>
</div>
<?php
}

function submit_leadership_form()
{
    if(!isset($_REQUEST['contactname']) || !isset($_REQUEST['position']) || !isset($_REQUEST['employer']) || !isset($_REQUEST['phone']) || !isset($_REQUEST['emai']))
        return false;

    if(empty($_REQUEST['contactname']) || empty($_REQUEST['position']) || empty($_REQUEST['employer']) || empty($_REQUEST['phone']) || empty($_REQUEST['emai']) || !filter_var($_REQUEST['emai'], FILTER_VALIDATE_EMAIL))
        return false;

    require_once(EMAIL_INCLUDE_ADDRESS.'class.phpmailer.php');

    $mail = new PHPMailer(); // defaults to using php "mail()"

    $mail->IsSendmail(); // telling the class to use SendMail transport

    $body  = "<p>New message from Leadership Website (Registration)</p>";
    $body .= "<p>Name: ".$_REQUEST['contactname']."</p>";
    $body .= "<p>Position: ".$_REQUEST['position']."</p>";
    $body .= "<p>Employer: ".$_REQUEST['employer']."</p>";
    $body .= "<p>Phone: ".$_REQUEST['phone']."</p>";
    $body .= "<p>Emai: ".$_REQUEST['emai']."</p>";

    $mail->SetFrom('info@leadership.edu.au.com', 'Leadership');

    //$mail->AddAddress("jodie.roker@monash.edu", "Admin");
    $mail->AddAddress("Michael@i-shop.com.au", "Manager");
    $mail->AddAddress("farshid@jobs4.com.au", "Developer");

    $mail->Subject = "New message from Leadership Website (Registration)";

    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

    $mail->MsgHTML($body);

    if(!$mail->Send())
        return false;
    else
        return true;
}

function contacts_form()
{
?>
<div class="leadershipform grid_custom">
    <div class='form-title'>Contact Us!</div>
    <form name="contacts" id="contacts" method="post">
        <div class="input"><input placeholder="Contact Name" name="name" id="name" value="" type="text" required="required" datadefault='Contact Name' /></div>
        <div class="input"><input placeholder="Email" name="email" id="email" value="" type="text" required="required" datadefault='Email' /></div>
        <div class="input"><input placeholder="Title" name="messagetitle" id="messagetitle" value="" type="text" datadefault='Title' /></div>
        <div class="input"><textarea placeholder="Message" name="message" id="message" datadefault='Message'></textarea></div>
        <div class="input"><input class='contactssubmit' type="submit" name='contactssubmit' id='contactssubmit' /></div>
    </form>
</div>
<?php
}

function submit_contacts_form()
{
    if(!isset($_REQUEST['name']) || !isset($_REQUEST['messagetitle']) || !isset($_REQUEST['message']) || !isset($_REQUEST['emai']))
        return false;

    if(empty($_REQUEST['name']) || empty($_REQUEST['messagetitle']) || empty($_REQUEST['message']) || empty($_REQUEST['emai']) || !filter_var($_REQUEST['emai'], FILTER_VALIDATE_EMAIL))
        return false;

    require_once(EMAIL_INCLUDE_ADDRESS.'class.phpmailer.php');

    $mail = new PHPMailer(); // defaults to using php "mail()"

    $mail->IsSendmail(); // telling the class to use SendMail transport

    $body  = "<p>New message from Leadership Website (Contact)</p>";
    $body .= "<p>Name: ".$_REQUEST['contactname']."</p>";
    $body .= "<p>Position: ".$_REQUEST['position']."</p>";
    $body .= "<p>Employer: ".$_REQUEST['employer']."</p>";
    $body .= "<p>Emai: ".$_REQUEST['emai']."</p>";
    $body .= "<p>Title: ".$_REQUEST['messagetitle']."</p>";
    $body .= "<p>Message: ".$_REQUEST['message']."</p>";

    $mail->SetFrom('info@leadership.edu.au.com', 'Leadership');

    //$mail->AddAddress("jodie.roker@monash.edu", "Admin");
    $mail->AddAddress("Michael@i-shop.com.au", "Manager");
    $mail->AddAddress("farshid@jobs4.com.au", "Developer");

    $mail->Subject = "New message from Leadership Website (Contact)";

    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

    $mail->MsgHTML($body);

    if(!$mail->Send())
        return false;
    else
        return true;
}
?>