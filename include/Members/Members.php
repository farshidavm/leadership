<?php
$Members = array();
$i = 0;

//John Bertrand
$Members[$i]['name']            = "John";
$Members[$i]['surname']         = "Bertrand";
$Members[$i]['profession']      = "1983 America's Cup Winning Captain";
$Members[$i]['priority']        = 1;
$Members[$i]['avatar']          = "johnBertrand_thumb.jpg";
$Members[$i]['description']     = "John Bertrand is an international sportsman, businessman and philanthropist.<br /><br />";
$Members[$i]['description']    .= "He is the Australian who skippered the winged keel Australia II to victory over Dennis Connor&#39;s Liberty to win the 1983 America&#39;s Cup, breaking 132 years of American domination - the longest-running record in the history of modern sport.<br /><br />";
$Members[$i]['description']    .= "The Confederation of Australian Sport voted the victory the &#8220;greatest team performance in 200 years of Australian sport&#8221;.<br /><br />";
$Members[$i]['description']    .= "A world champion and Olympic bronze medallist, John represented Australia in five America&#39;s Cups and two Olympic Games.<br /><br />";
$Members[$i]['description']    .= "Within the world of the America&#39;s Cup, he was involved in all areas of activity from skipper of Australia II to chairman of One Australia.<br /><br />";
$Members[$i]['description']    .= "John is both a successful businessman and philanthropist.<br /><br />";
$Members[$i]['description']    .= "Over his business career, he established successful businesses in the marine, property and digital media industries.<br /><br />";
$Members[$i]['description']    .= "He is chairman of the Prime Minister&#39;s Alannah and Madeline Foundation. The Foundation is a world leader in anti-bullying and cyber-safety programs within Australian schools.<br /><br />";
$Members[$i]['description']    .= "John is also chairman of the Sport Australia Hall of Fame, whose inductees include Sir Don Bradman, Herb Elliot, Dawn Fraser, and Rod Laver. In 2008, John was named Melbournian of the Year ,and in 2011 he received Monash University&#39;s Distinguished Alumni Lifetime Achievement Award. Both awards were received for his philanthropic and community leadership.<br /><br />";
$Members[$i]['description']    .= "He holds a mechanical engineering degree from Monash University and a Master of Science from MIT in Boston. He is a Vice-Chancellors Professorial Fellow of Monash University.<br /><br />";
$Members[$i]['description']    .= "John was chairman of selectors of the #1 world ranked Australian Olympic sailing team for London. He mentors many aspiring young Olympians.<br /><br />";
$Members[$i]['description']    .= "John continues to compete at the highest levels in sailing, including winning the 2010 World Etchells Class sailing championship.<br /><br />";
$Members[$i]['summary']         = "John Bertrand is an international sportsman, businessman and philanthropist.";
$i++;

//Angus Campbell
$Members[$i]['name']            = "Maj. Gen.";
$Members[$i]['surname']         = "Angus Campbell";
$Members[$i]['profession']      = "Deputy Chief of Army";
$Members[$i]['priority']        = 0;
$Members[$i]['avatar']          = "angusCampbell_thumb.jpg";
$Members[$i]['description']     = "Angus Campbell attended the Royal Military College, Duntroon, from 1981 to 1984. He graduated into the Infantry Corps and initially served as a platoon commander in the 3rd Battalion (Parachute), the Royal Australian Regiment.<br /><br />";
$Members[$i]['description']    .= "His regimental service then included troop and squadron command in the Special Air Service Regiment. In 2001-02, he commanded the 2nd Battalion, The Royal Australian Regiment. He led the 2nd Battalion Group deployment to East Timor as a component of the United Nations Transitional Administration in East Timor.<br /><br />";
$Members[$i]['description']    .= "He has also served in a range of staff appointments, including as aide-de-camp to the Chief of Army, as a strategic policy officer in Army Headquarters, an instructor at the Australian Command and Staff College and chief of staff to the Chief of the Defense Force.<br /><br />";
$Members[$i]['description']    .= "In late 2005, he joined the Department of Prime Minister and Cabinet as a first assistant secretary to head the Office of National Security. He was subsequently promoted to deputy secretary and appointed to the position of Deputy National Security Adviser. In these roles he was responsible for preparing advice to the Prime Minister on national security issues and coordinating the development of whole-of-government national security policy.<br /><br />";
$Members[$i]['description']    .= "He returned to the Australian Defense Force in early 2010, and was appointed to the rank of Major General. He led the Military Strategic Commitments staff in defense headquarters until January 2011, when he assumed command of all Australian forces deployed in the Middle East Area of Operations. He was appointed Deputy Chief of Army on 27 February 2012.<br /><br />";
$Members[$i]['description']    .= "He has a Bachelor of Science (Honors) from the University of New South Wales, a Master of Philosophy in International Relations from Cambridge University and he is a graduate of the Australian Army Command and Staff College.<br /><br />";
$Members[$i]['description']    .= "Major General Campbell is married to Stephanie and they have two teenage children.";
$Members[$i]['summary']         = "Angus Campbell attended the Royal Military College, Duntroon, from 1981 to 1984. He graduated into the Infantry Corps and initially served as a platoon commander in the 3rd Battalion (Parachute), the Royal Australian Regiment.";
$i++;

//Bob Hawke
$Members[$i]['name']            = "Bob";
$Members[$i]['surname']         = "Hawke";
$Members[$i]['profession']      = "Former Prime Minister of Australia";
$Members[$i]['priority']        = 0;
$Members[$i]['avatar']          = "bobHawke_thumb.jpg";
$Members[$i]['description']     = "The Hon Bob Hawke AC was born in South Australia in 1929. After completing a Bachelor of Laws and Bachelor of Arts (Economics) at the University of Western Australia, he studied at Oxford University under a Rhodes Scholarship, graduating with a Bachelor of Letters.<br /><br />";
$Members[$i]['description']    .= "In 1956 he returned to Australia to take up a research scholarship at the Australian National University, and in 1958 became research officer and advocate with the Australian Council of Trade Unions (ACTU). He was ACTU President from 1970 to 1980. From 1973 to 1978, he was also president of the Australian Labor Party.<br /><br />";
$Members[$i]['description']    .= "In 1980 he was elected to the Federal Parliament, and in February 1983 became the Leader of the Opposition. He led the Labor Party to victory in the general election in March 1983. <br /><br />";
$Members[$i]['description']    .= "Bob Hawke won three successive elections, and became Australia&#39;s longest serving Labor Prime Minister. He ceased to be Prime Minister in December 1991 and resigned from Parliament in February 1992. In August 2009 Mr Hawke was given a national life membership of the Australian Labor Party.<br /><br />";
$Members[$i]['description']    .= "His post-parliamentary career has included adjunct professor in the Research Schools of Pacific Studies and Social Sciences at the Australian National University; honorary visiting professor in industrial relations at the University of Sydney, membership of the advisory council of the Institute for International Studies at Stanford University and chairman of the Committee of Experts on Membership of Education International.<br /><br />";
$Members[$i]['description']    .= "He is an honorary fellow at University College, Oxford, and holds honorary degrees from the University of Western Australia, Nanjing University of China, the Hebrew University of Jerusalem, the University of New South Wales, the University of South Australia, Oxford University and Rikkyo University, Tokyo.<br /><br />";
$Members[$i]['description']    .= "Mr Hawke was made a Companion of the Order of Australia (AC) in 1979. In 2009 he was awarded the highest award for non-citizens by the Papuan New Guinea Government, the Grand Companion of the Order of Logohu (GCL). In 2012 Mr Hawke received the Grand Cordon of the Order of the Rising Sun from the Government of Japan.<br /><br />";
$Members[$i]['description']    .= "Current honorary positions include: member of the advisory council, the United States Studies Centre at the University of Sydney; member of the council of advisers of the Boao Forum for Asia; member of the international advisory council of the Australian American Leadership Dialogue; chairman of the Hawke Research Institute advisory board at the Bob Hawke Prime Ministerial Centre, University of South Australia; member of the advisory board, Deliberative Issues Australia; patron of the Australian Institute of Employment Rights, and patron of Indigenous Engineering Aid.<br /><br />";
$Members[$i]['summary']         = "The Hon Bob Hawke AC was born in South Australia in 1929. After completing a Bachelor of Laws and Bachelor of Arts (Economics) at the University of Western Australia, he studied at Oxford University under a Rhodes Scholarship, graduating with a Bachelor of Letters.";
$i++;

//Dr Fiona Wood
$Members[$i]['name']            = "Dr Fiona";
$Members[$i]['surname']         = "Wood";
$Members[$i]['profession']      = "Director Royal Perth Hospital<br />Burns Unit";
$Members[$i]['priority']        = 0;
$Members[$i]['avatar']          = "fionaWood_thumb.jpg";
$Members[$i]['description']     = "Fiona Wood FRCS, FRACS, AM, Winthrop Professor School of Surgery, UWA<br /><br />";
$Members[$i]['description']    .= "2005 Australian of the Year Fiona Wood has been a burns surgeon and researcher for the past 20 years and is Director of the Burns Service of Western Australia (BSWA). She is a Consultant Plastic Surgeon at Royal Perth Hospital and Princess Margaret Hospital for Children, Co-founder of the first skin cell laboratory in WA, Winthrop Professor in the School of Surgery at The University of Western Australia, and Co-founder and Chair, of the Fiona Wood Foundation (formerly The McComb Foundation).<br /><br />";
$Members[$i]['description']    .= "Fiona&#39;s mantra is &#8220;The quality of outcome must be worth the pain of survival&#8221;. As such, Fiona is committed to the Foundation&#39;s vision:<br /><br />";
$Members[$i]['description']    .= "To deliver scarless healing - in mind, body and spirit - to all burn injury survivors.<br /><br />";
$Members[$i]['description']    .= "In 2002, Dr Wood and her team worked day and night to care for badly burned victims of the Bali bombings. It was during this time that the &#8220;spray-on skin&#8221; cell technology Dr Wood pioneered for use in treating burns victims grabbed international headlines. In recognition of her work with the Bali bombing victims Fiona was named a Member of the Order of Australia in 2003. That same year the Australian Medical Association bestowed upon Fiona the &#8220;Contribution to Medicine&#8221; Award.<br /><br />";
$Members[$i]['description']    .= "Fiona was named West Australian of the Year in 2004, as well as receiving nominations that same year as a National Living Treasure and Australian Citizen of the Year. Fiona was again named West Australian of the Year in 2005 and received the honour of being named Australian of the Year for 2005.<br /><br />";
$Members[$i]['description']    .= "In 2005, Fiona, along with McComb Foundation co-founder Marie Stoner, won the Clunies Ross Award for their contribution to Medical Science in Australia. From 2005 to 2010, Fiona was voted Australia&#39;s Most Trusted Person in the annual Reader&#39;s Digest survey.<br /><br />";
$Members[$i]['description']    .= "Her unwavering dedication to burns survivors, as well as her commitment to improving and continually evolving the treatment of burn injury, has earned her a reputation as one of the world&#39;s leading burn experts.<br /><br />";
$Members[$i]['description']    .= "Fiona is married to fellow surgeon Tony Kierath and is mother to four boys and two girls.<br /><br />";
$Members[$i]['summary']         = "Fiona Wood FRCS, FRACS, AM, Winthrop Professor School of Surgery, UWA";
$i++;

//Prof. Christine Kilpatrick
$Members[$i]['name']            = "Prof. Christine";
$Members[$i]['surname']         = "Kilpatrick";
$Members[$i]['profession']      = "CEO of the Royal Children&#39;s Hospital";
$Members[$i]['priority']        = 0;
$Members[$i]['avatar']          = "christineKilpatrick_thumb.jpg";
$Members[$i]['description']     = "Professor Kilpatrick graduated from the medical school at the University of Melbourne and went on to pursue a career in neurology, specialising in epilepsy. She worked in both public and private practice as well as an academic career at the Royal Melbourne Hospital where she established and led its epilepsy program.<br /><br />";
$Members[$i]['description']    .= "Christine was appointed Chair of the RMS Senior Medical Staff in 1999. In 2000, she became a member of the Women's and Children's Health Board, and a year later became its deputy chairman.<br /><br />";
$Members[$i]['description']    .= "In the early 2000s, Professor Kilpatrick&#39;s career developed from neurological practice into hospital management. She was appointed Executive Director Medical Services, Melbourne Health, and later Executive Director Royal Melbourne Hospital. She became CEO of the Royal Children's Hospital Melbourne in August 2008.<br /><br />";
$Members[$i]['description']    .= "Professor Kilpatrick holds a Doctor of Medicine and a Master of Business Administration from the University of Melbourne. In addition to her professional responsibilities, she has also chaired the Victorian Quality Council in Healthcare and the Methodist Ladies College Board. She was awarded a Centenary Medal for her contribution to health care in 2003.<br /><br />";
$Members[$i]['summary']         = "Professor Kilpatrick graduated from the medical school at the University of Melbourne and went on to pursue a career in neurology, specialising in epilepsy. She worked in both public and private practice as well as an academic career at the Royal Melbourne Hospital where she established and led its epilepsy program. ";
$i++;

//Paul Roos
$Members[$i]['name']            = "Paul";
$Members[$i]['surname']         = "Roos";
$Members[$i]['profession']      = "Premiership winning coach";
$Members[$i]['priority']        = 0;
$Members[$i]['avatar']          = "paulRoose_thumb.jpg";
$Members[$i]['description']     = "When Sydney Swans coach and 2005 Australian Sports Coach of the Year Paul Roos placed his hands on the AFL premiership trophy in 2005, it signaled the end of a 72-year drought for the club.<br /><br />";
$Members[$i]['description']    .= "With more than 92,000 fans watching on at the Melbourne Cricket Ground and hundreds of thousands more glued to television screens around the world, this was Roos&#39; moment of glory.<br /><br />";
$Members[$i]['description']    .= "The former 356-match AFL superstar and Australian Hall of Fame member had just witnessed a nail-biting final quarter in the grand final that saw his beloved Swans come from behind to snatch an historic 58-54 victory over the West Coast Eagles.<br /><br />";
$Members[$i]['description']    .= "The Roos factor in the Swans triumph was generously acknowledged in post-match tributes.<br /><br />";
$Members[$i]['description']    .= "One from Peter Schwab, the former triple Hawthorn premiership player and later the Hawks&#39; club coach, said it for all. &#8220;The greatest strength in Roos' coaching,&#8221; he observed the next day, &#8220;appears to be his ability to make a group of players believe that everything is about and for the team&#8221;.<br /><br />";
$Members[$i]['description']    .= "In 2006, Roos&#39; Swans made it to the MCG again on Grand Final Day, but were beaten by one point by West Coast. Roos' true spirit was revealed that day when he accepted the defeat the same way he treated the victory the year before - the mark of a great sportsman.<br /><br />";
$Members[$i]['description']    .= "Following an outstanding career as a player, Roos answered a call to fill the vacant senior coaching position at the Swans midway through the 2002 season.<br /><br />";
$Members[$i]['description']    .= "Full-time assistant coach since 2001, he was subsequently appointed to the club&#39;s senior coaching position for a three-year term from 2003. Remarkably, he guided the Swans into the playoffs in 2003, his talents acknowledged with his naming as AFL Coach of the Year.<br /><br />";
$Members[$i]['description']    .= "Roos played 87 games for the Swans after joining the Sydney Cricket Ground-headquartered club in 1995 following a star-studded career with the old Fitzroy club - now the Brisbane Lions - including six seasons as captain.<br /><br />";
$Members[$i]['description']    .= "During his career with Fitzroy, he was a member of the Victorian representative team 10 years running, two as captain. He was also named in the All-Australian team on seven occasions, twice as skipper.<br /><br />";
$Members[$i]['description']    .= "An extremely mobile player with expansive marking and kicking skills, Roos was runner-up in the Brownlow Medal - AFL&#39;s highest individual player award - in 1986 after finishing third the year before. He also won the E J Whitten Medal, awarded to the best Victorian player in State of Origin football, in 1985 and 1988.<br /><br />";
$Members[$i]['description']    .= "Since standing down as Swans coach, Roos has taken on the role of coach of the Swans Academy. He has also made his mark as a columnist with News Limited newspapers and as a member of Fox Sports AFL commentary team.<br /><br />";
$Members[$i]['summary']         = "When Sydney Swans coach and 2005 Australian Sports Coach of the Year Paul Roos placed his hands on the AFL premiership trophy in 2005, it signaled the end of a 72-year drought for the club.";
$i++;

/*
//Radek Sali
$Members[$i]['name']            = "Radek";
$Members[$i]['surname']         = "Sali";
$Members[$i]['profession']      = "CEO - Swisse";
$Members[$i]['priority']        = 0;
$Members[$i]['avatar']          = "radekSali_thumb.jpg";
$Members[$i]['description']     = "Biography to follow";
$Members[$i]['summary']         = "Biography to follow";
$i++;
*/
?>