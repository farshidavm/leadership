<?php
$Conversations = array();
$i = 0;

//Bob Hawke
$Conversations[$i]['image'] = "bobHawke_thumb.png";
$Conversations[$i]['alt']   = "Bob Hawke";
$i++;

//Paul Roose
$Conversations[$i]['image'] = "paulRoose_thumb.png";
$Conversations[$i]['alt']   = "Paul Roose";
$i++;

//Angus Campbell
$Conversations[$i]['image'] = "angusCampbell_thumb.png";
$Conversations[$i]['alt']   = "Angus Campbell";
$i++;

//Fiona Wood
$Conversations[$i]['image'] = "fionaWood_thumb.png";
$Conversations[$i]['alt']   = "Fiona Wood";
$i++;

//Christine Kilpatrick
$Conversations[$i]['image'] = "christineKilpatrick_thumb.png";
$Conversations[$i]['alt']   = "Christine Kilpatrick";
$i++;

//Alan Joyce
$Conversations[$i]['image'] = "alanJoyce_thumb.png";
$Conversations[$i]['alt']   = "Alan Joyce ";
$i++;
?>