<?php
    class Boxes
    {
        public function get_footermenu()
        {
            /*
            echo "<div class='alink'><a alt='John Bertand Leadership Series' title='John Bertand Leadership Series' href='".ROOT_HTTP_SERVER."'>HOME</a></div>";
            echo "<div class='alink'><a alt='John Bertand Leadership Series - Essence of leadership' title='John Bertand Leadership Series - Essence of leadership' href='".ROOT_HTTP_SERVER."essence'>".strtoupper("Essence of leadership")."</a></div>";
            echo "<div class='alink'><a alt='John Bertand Leadership Series - Leadership series' title='John Bertand Leadership Series - Leadership series' href='".ROOT_HTTP_SERVER."series'>".strtoupper("Leadership series")."</a></div>";
            echo "<div class='alink'><a alt='John Bertand Leadership Series - Participant program' title='John Bertand Leadership Series - Participant program' href='".ROOT_HTTP_SERVER."participant'>".strtoupper("Participant program")."</a></div>";
            echo "<div class='alink'><a alt='John Bertand Leadership Series - Public events' title='John Bertand Leadership Series - Public events' href='".ROOT_HTTP_SERVER."events'>".strtoupper("Public events")."</a></div>";
            echo "<div class='alink'><a alt='John Bertand Leadership Series - Speakers' title='John Bertand Leadership Series - Speakers' href='".ROOT_HTTP_SERVER."speakers'>".strtoupper("Speakers")."</a></div>";
            echo "<div class='alink'><a alt='John Bertand Leadership Series - Contact us' title='John Bertand Leadership Series - Contact us' href='".ROOT_HTTP_SERVER."contacts'>".strtoupper("Contact us")."</a></div>";
            //*/
        }

        public function get_footerpartner()
        {
            echo "<div class=''><a alt='John Bertand Leadership Series - ' title='John Bertand Leadership Series - ' target='_blank' href='http://www.monash.edu.au/'><img src='".IMAGE_FOLDER."monash_footer_logo.png' /></a></div>";
            echo "<div class=''><a alt='John Bertand Leadership Series - ' title='John Bertand Leadership Series - ' target='_blank' href='http://www.google.com.au'><img src='".IMAGE_FOLDER."google_footer_logo.png' /></a></div>";
            echo "<div class=''><a alt='John Bertand Leadership Series - ' title='John Bertand Leadership Series - ' target='_blank' href='http://www.unijobs.com.au'><img src='".IMAGE_FOLDER."unijobs_footer_logo.png' /></a></div>";
            echo "<div class=''><a alt='John Bertand Leadership Series - ' title='John Bertand Leadership Series - ' target='_blank' href='http://www.globalacademictalent.com'><img style='width: 234px' src='".IMAGE_FOLDER."gaTalent_footer_logo.png' /></a></div>";
        }

        public function get_footersocial()
        {
            /*
            echo "<div class='social'><a alt='' title='' target='_blank' href=''><img src='".IMAGE_FOLDER."linkedIn_grey.png' /> <span>".strtoupper("linkedin")."</span></a></div>";
            echo "<div class='social'><a alt='' title='' target='_blank' href=''><img src='".IMAGE_FOLDER."twitter_grey.png' /> <span>".strtoupper("twitter")."</span></a></div>";
            echo "<div class='social'><a alt='' title='' target='_blank' href=''><img src='".IMAGE_FOLDER."facebook_grey.png' /> <span>".strtoupper("facebook")."</span></a></div>";
            echo "<div class='social'><span class='leaders'>LEADERS</span> <a class='logjoin' href=''>SIGN IN</a> <span class='devider'>|</span> <a class='logjoin' href=''>JOIN</a></div>";
            */
        }

        public function get_mainmenu()
        {
            $class = "";

            echo "<div class='menu-tab container_24'>";
                echo "<span class='menubutton grid_custom singleline ".(strstr(basename(curPageURL()), 'leadership') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series' title='John Bertand Leadership Series' href='".ROOT_HTTP_SERVER."'>Home</a></span>";
                echo "<span class='menubutton grid_custom doubleline ".(strstr(basename(curPageURL()), 'essence') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series - Essence of leadership' title='John Bertand Leadership Series - Essence of leadership' href='".ROOT_HTTP_SERVER."essence'>Essence of leadership</a></span>";
                echo "<span class='menubutton grid_custom doubleline ".(strstr(basename(curPageURL()), 'series') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series - Leadership series' title='John Bertand Leadership Series - Leadership series' href='".ROOT_HTTP_SERVER."series'>Leadership series</a></span>";
                echo "<span class='menubutton grid_custom singleline ".(strstr(basename(curPageURL()), 'speakers') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series - Speakers' title='John Bertand Leadership Series - Speakers' href='".ROOT_HTTP_SERVER."speakers'>Speakers</a></span>";                 
                /*
                echo "<span class='grid_custom'>";
                    echo "<span dataparent='series' class='havesubmenu menubutton ".(strstr(basename(curPageURL()), 'series') ? "active" : "inactive" ).$class."'><a href='".ROOT_HTTP_SERVER."series'>Leadership series</a></span>";
                    echo "<span class='series menu-tab-submenu hidden'>";
                        echo "<ul>";
                            echo "<li><span class='submenubutton grid_custom ".(strstr(basename(curPageURL()), 'essence') ? "active" : "inactive" ).$class."'><a href='".ROOT_HTTP_SERVER."essence'>Essence of leadership</a></span></li>";
                        echo "</ul>";
                    echo "</span>";
                echo "</span>";
                */
                echo "<span class='menubutton grid_custom doubleline ".(strstr(basename(curPageURL()), 'participant') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series - Participant program' title='John Bertand Leadership Series - Participant program' href='".ROOT_HTTP_SERVER."participant'>Participant program</a></span>";
                echo "<span class='menubutton grid_custom singleline ".(strstr(basename(curPageURL()), 'event') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series - Public events' title='John Bertand Leadership Series - Public events' href='".ROOT_HTTP_SERVER."events'>Public events</a></span>";
                echo "<span class='menubutton grid_custom singleline ".(strstr(basename(curPageURL()), 'contacts') ? "active" : "inactive" ).$class."'><a alt='John Bertand Leadership Series - Contact us' title='John Bertand Leadership Series - Contact us' href='".ROOT_HTTP_SERVER."contacts'>Contact us</a></span>";
            echo "</div>";
            echo "<div class='clear'></div>";
            /*
            echo "<div id='learnmorebuttom' class='grid_2'><img src='".IMAGE_FOLDER."learnMore_btn.png' /></div>";
            echo "<div id='daycount' class='grid_2'>";
                echo "<div class='time'>8</div>";
                echo "<div class='time'>11</div>";
                echo "<div class='time'>23</div>";
                echo "<div class='time'>22</div>";
                echo "<div class='time timebuttom'>DAYS</div>";
                echo "<div class='time timebuttom'>HOURS</div>";
                echo "<div class='time timebuttom'>MINS</div>";
                echo "<div class='time timebuttom'>SECS</div>";
            echo "</div>";
            */
        }

        public static function members_slider($Members)
        {
            echo "<ul id='members_slider' class='jcarousel-skin-tango'>";
            foreach($Members as $key => $member):
                echo "<li>";
                    echo "<a href='".ROOT_HTTP_SERVER."speakers'>";
                        echo "<div class='grid_68 avatar'><img src='".AVATAR_MEMBERS_IMAGE_FOLDER.$member['avatar']."'; /></div>";
                        echo "<div>";
                            echo "<h1>".$member['name']." ".$member['surname']."</h1>";
                            echo "<h2>".$member['profession']."</h2>";
                        echo "</div>";
                    echo "</a>";
                echo "</li>";
            endforeach;
            echo "</ul>";
        }

        public function event()
        {
            echo "<div class='eventbox box'>";
                echo "<img alt='' title='' src='".BOX_FOLDER."events_1.png' />";
                echo "<img alt='' title='' src='".BOX_FOLDER."events_2.png' />";
                echo "<img alt='' title='' src='".BOX_FOLDER."events_3.png' />";
                echo "<img alt='' title='' src='".BOX_FOLDER."events_4.png' />";
            echo "</div>";
        }

        public function follow()
        {
            echo "<div class='followbox box'>";
                echo "<img alt='' title='' src='".BOX_FOLDER."followTheSeries_content.png' />";
                echo "<div class='follow facebook'></div>";
                echo "<div class='follow twitter'></div>";
                echo "<div class='follow linkedin'></div>";
            echo "</div>";
        }

        public function contact()
        {
            echo "<div class='contactwbox box'>";
                echo "<img alt='' title='' src='".BOX_FOLDER."contactDetails.png' />";
                echo "<div><a class='emaillink bolder' href='mailto:jodie.roker@monash.edu'>Email: jodie.roker@monash.edu</a></div>";
            echo "</div>";
        }

        public function pageBoxLeft($page)
        {
            switch($page):
                default:
                    $this->event();
                    $this->follow();
                    $this->contact();
                    break;
            endswitch;
        }
    }
?>