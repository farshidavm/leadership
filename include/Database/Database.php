<?php
    class Database
    {
        private $dsn;
        private $user;
        private $password;
        private $options;
        private $connection;
        private $error;
        private $lastInsertId;

        public function __construct()
        {
            $this->error = null;
            $this->options = array(PDO::ATTR_AUTOCOMMIT => FALSE);

            $this->dsn = 'mysql:dbname=ujcom_global;host=174.120.96.92';
            $this->user = 'ujcom_ujcom';
            $this->password = 'p$UPqJ,7i?px';

        	if($_SERVER['SERVER_NAME'] == "localhost")
        	{
                $this->dsn = 'mysql:dbname=unijobs;host=localhost';
                $this->user = 'root';
                $this->password = '';
			}

            $this->connect();
        }

        public function __destruct()
        {
            unset($this->connection);
        }

        private function connect()
        {
            try
            {
                $this->connection = new PDO($this->dsn, $this->user, $this->password, $this->options);
            }
            catch (PDOException $e)
            {
                $this->error = $this->connection->errorInfo();
                $this->connection = false;
            }
        }

        public function get_error()
        {
            echo "<pre>";
            print_r($this->error);
            echo "</pre>";
        }

        public function exec($query)
        {
            if(empty($query)):
                $this->error = "Empty query inserted";
                return false;
            endif;

            try
            {
                $this->connection->beginTransaction();
                $count = $this->connection->exec($this->connection->prepare($query)->queryString);
                $this->lastInsertId = $this->connection->lastInsertId();
                $this->connection->commit();
            }
            catch(PDOExecption $e)
            {
                $this->connection->rollback();
                $this->error = $this->connection->errorInfo();
                return false;
            }

            return $count." rows affected";
        }

        public function query($query, $fetch = 'all')
        {
            if(empty($query)):
                $this->error = "Empty query inserted";
                return false;
            endif;

            $query = $this->connection->prepare($query);
            if(!$query->execute()):
                $this->error = $this->connection->errorInfo();
                return false;
            endif;

            $result = array();
            if($fetch == 'single')
                $result =  $query->fetch(PDO::FETCH_ASSOC);
            else
                $result =  $query->fetchAll(PDO::FETCH_ASSOC);

            unset($query);
            return $result;
        }

        public function meta($table)
        {
            if(empty($table)):
                $this->error = "Empty table entered";
                return false;
            endif;

            $query = $this->connection->prepare("SELECT * FROM ".$table." LIMIT 1");
            if(!$query->execute()):
                $this->error = $this->connection->errorInfo();
                return false;
            endif;

            $result = $query->fetch(PDO::FETCH_ASSOC);
            unset($query);

            if(empty($result))
                return false;

            $columns = array();
            foreach($result as $key => $value):
                $columns[] = $key;
            endforeach;

            return $columns;
        }

        public function lastInsertId()
        {
            return $this->lastInsertId;
        }

        public function quote($string)
        {
            return $this->connection->quote($string);
        }
    }
?>
