<?php
class Check
{
    public static function Profile($data = array())
    {
        if(empty($data))
            return false;

        if(!is_array($data))
            return false;

        if(isset($_SESSION['ERROR'])) unset($_SESSION['ERROR']);
        if(isset($_SESSION['ERROR_MSG'])) unset($_SESSION['ERROR_MSG']);

    	if (empty($data['firstname']))
        {
    		$_SESSION['ERROR']['txtName'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your given name.";
    	}
    	if (empty($data['lastname']))
        {
    		$_SESSION['ERROR']['txtLastName'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your family name.";
    	}
    	if (empty($data['email']))
        {
    		$_SESSION['ERROR']['txtEmail'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your email address.";
    	}
        if (User::isExisting($data['email']) && !is_a($GLOBALS['loggedinUser'], "User"))
        {
    		$_SESSION['ERROR']['txtEmail'] = "invalid";
    		$_SESSION['ERROR_MSG'][] = "Email address exists.";
    	}
    	if (empty($data['password']))
        {
    		$_SESSION['ERROR']['txtPassword'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your password.";
    	}
    	if (empty($data['repassword']))
        {
    		$_SESSION['ERROR']['txtRePassword'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your confirmation password.";
    	}
        if($data['repassword'] != $data['password'])
        {
    		$_SESSION['ERROR']['txtRePassword'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please correct your confirmation password.";
    	}
    	if (empty($data['speciality']))
        {
    		$_SESSION['ERROR']['txtSpeciality'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your speciality.";
    	}
    	if (empty($data['location']))
        {
    		$_SESSION['ERROR']['txtLocation'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your city.";
    	}
    	if (empty($data['country']))
        {
    		$_SESSION['ERROR']['txtLocation'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your country.";
    	}

        if (!isset($_SESSION['ERROR']) && !isset($_SESSION['ERROR_MSG']))
            return true;
        else
            return false;
    }

    public static function Login($data)
    {
        if(empty($data))
            return false;

        if(!is_array($data))
            return false;

        if(isset($_SESSION['ERROR'])) unset($_SESSION['ERROR']);
        if(isset($_SESSION['ERROR_MSG'])) unset($_SESSION['ERROR_MSG']);

    	if(empty($data['email']))
        {
    		$_SESSION['ERROR']['txtEmail'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your email address.";
    	}
        if(!User::isExisting($data['email']))
        {
    		$_SESSION['ERROR']['txtEmail'] = "invalid";
    		$_SESSION['ERROR_MSG'][] = "Email address does not exists.";
    	}
    	if(empty($data['password']))
        {
    		$_SESSION['ERROR']['txtPassword'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your password.";
    	}
        if(!User::isUser($data['email'], $data['password']))
        {
    		$_SESSION['ERROR']['txtLogin'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "User does not exists.";
        }

        if (!isset($_SESSION['ERROR']) && !isset($_SESSION['ERROR_MSG']))
            return true;
        else
            return false;
    }

    public static function Discussion($groupid, $data)
    {
        if(empty($data))
            return false;

        if(!is_array($data))
            return false;

        if(empty($groupid))
            return false;

        if(!is_numeric($groupid))
            return false;

        if(isset($_SESSION['ERROR'])) unset($_SESSION['ERROR']);
        if(isset($_SESSION['ERROR_MSG'])) unset($_SESSION['ERROR_MSG']);

    	if(empty($data['title']))
        {
    		$_SESSION['ERROR']['title'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your discussion title.";
    	}
    	if(empty($data['detail']))
        {
    		$_SESSION['ERROR']['detail'] = "empty";
    		$_SESSION['ERROR_MSG'][] = "Please input your discussion detail.";
    	}
        if(Group::ifExistsDiscussion($groupid, $data))
        {
    		$_SESSION['ERROR']['title'] = "invalid";
    		$_SESSION['ERROR_MSG'][] = "Discussion exists.";
    	}

        if (!isset($_SESSION['ERROR']) && !isset($_SESSION['ERROR_MSG']))
            return true;
        else
            return false;
    }
}
?>