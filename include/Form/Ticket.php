<?php
require_once "./top.php";
function ticket_form()
{
?>
<div class="ticketform">
    <form name="ticket" id="ticket" method="post">
        <div class="input"><input datadefault='Contact Name' name="contactname" id="contactname" value="" type="text" /></div>
        <div class="input"><input datadefault='Position' name="position" id="position" value="" type="text" /></div>
        <div class="input"><input datadefault='Employer' name="employer" id="employer" value="" type="text" /></div>
        <div class="input"><input datadefault='Phone' name="phone" id="phone" value="" type="text" /></div>
        <div class="input"><input datadefault='Email' name="email" id="email" value="" type="text" /></div>
        <div class="input">
            <img id='ticketsubmit' src="<?php echo IMAGE_FOLDER; ?>getTicket_btn.png" />
            <span class='group'><a href=''>Group Booking</a></span>
        </div>
    </form>
</div>
<?php
}
require_once "./down.php";
?>