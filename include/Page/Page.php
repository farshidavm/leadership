<?php
    class Page
    {
		private $title;
		private $charset;
		private $css;
		private $altcss;
		private $js;
		private $inlinejs;
		private $meta;
		private $favicon;
		private $base_path;
		private $notificationBar;
        public $Browser;

        private $Boxes;
        private $RightBoxes;
        private $LeftBoxes;

		function __construct()
        {
			$this->title = "";
			$this->css = array();
			$this->altcss = array();
			$this->js = array();
			$this->inlinejs = array();
			$this->meta = array();
			$this->favicon = array();
			$this->charset = "utf-8";

		   	$this->linkJSFile("http://code.jquery.com/jquery-1.8.2.js");
		   	$this->linkJSFile(JS_FOLDER.'jquery-1.8.3.min.js');
            $this->linkJSFile(JS_FOLDER."jquery.jcarousel.min.js");
			$this->linkJSFile(JS_FOLDER.'common.js');

            $this->Boxes = new Boxes();
            $this->Browser = new Browser;
		}

		public function linkCSS($file, $media = "all", $title = "", $is_alternate = false)
        {
			if ($is_alternate)
				$this->altcss[$file][$media] = $title;
            else
				$this->css[$file][$media] = $title;
		}

		public function __get($attr)
        {
			return $this->$attr;
		}

		public function linkJSFile($file)
        {
			$this->js[$file] = "text/javascript";
		}

		public function linkJS($file)
        {
			$this->js[$file] = "text/javascript";
		}

		public function addJS($name, $content)
        {
			$this->inlinejs[$name] = $content;
		}

		public function linkFavicon($file, $filetype, $type="shortcut icon")
        {
			$this->favicon[$type][0] = $file;
			$this->favicon[$type][1] = $filetype;
		}

		public function setTitle($title)
        {
			$this->title = $title;
		}

		public function setCharset($charset)
        {
			$this->charset = $charset;
		}

		public function setBase($path)
        {
			$this->base_path = $path;
		}

		public function getBase()
        {
			return $this->base_path;
		}

		public function displayHead()
        {
			echo "<!DOCTYPE html PUBLIC";
			echo "\"-//W3C//DTD XHTML 1.0 Strict//EN\"";
			echo "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";

			echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">";

			echo "<head>";

			if ($this->base_path != null):
				echo "<!-- Base Path -->";
				echo "<base href=\"" . $this->base_path . "\" />";
			endif;

			echo "<!-- Page Title -->";
			echo "<title>" . $this->title . "</title>";

			echo "<!-- META Tags -->";
			echo "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=" . $this->charset . "\" />";
			echo "<meta http-equiv=\"Content-Language\" content=\"en\" />";
			echo "<meta name='language' content='English' />";
			echo "<meta name='description' content='Monash Leadership' />";
			echo "<meta name='keywords' content='john bertrand, leadership, john bertrand leadership, explore essence of leadership' />";
			echo "<meta name='keywords' content='monash, leadership, participants, speaker' />";
			echo "<meta name=\"ROBOTS\" content=\"INDEX, FOLLOW\" />";

			if (count($this->meta) > 0)
            {
				foreach($this->meta as $name => $content):
					echo "<meta";
					echo " name=\"" . $name . "\"";
					echo " content=\"" . $content . "\"";
					echo " />";
				endforeach;
			}

			if (count($this->favicon) > 0)
            {
				echo "<!-- Favicon -->";
				foreach($this->favicon as $type => $file):
					echo "<link";
					echo " rel=\"" . $type . "\"";
					echo " type=\"" . $file[1] . "\"";
					echo " href=\"" . $file[0] . "\"";
					echo " />";
				endforeach;
			}

			if (count($this->css) > 0):
				echo "<!-- Cascading Style Sheets -->";
				foreach($this->css as $filename => $files):
					foreach ($files as $media => $title):
						echo "<link";
						echo " rel=\"stylesheet\"";
						echo " href=\"" . $filename . "\"";
						echo " type=\"text/css\"";
						echo " media=\"" . $media . "\"";
						if (strlen($title) > 0)
							echo " title=\"" . $title . "\"";
						echo " />";
					endforeach;
				endforeach;
			endif;

			if (count($this->altcss) > 0):
				echo "<!-- Alternate Cascading Style Sheets -->";
				foreach($this->altcss as $filename => $files):
					foreach ($files as $media => $title):
						echo "<link";
						echo " rel=\"alternate stylesheet\"";
						echo " href=\"" . $filename . "\"";
						echo " type=\"text/css\"";
						echo " media=\"" . $media . "\"";
						if (strlen($title) > 0)
							echo " title=\"" . $title . "\"";
						echo " />";
					endforeach;
				endforeach;
			endif;

			if (count($this->js) > 0):
				echo "<!-- External Javascript -->";
				foreach($this->js as $filename => $type):
					echo "<script";
					echo " src=\"" . $filename . "\"";
					echo " type=\"" . $type . "\"";
					echo "></script>";
				endforeach;
			endif;

			if (count($this->inlinejs) > 0):
				echo "<!-- Inline Javascript -->";
				echo "<script";
				echo " type=\"text/javascript\"";
				echo ">";
				foreach($this->js as $filename => $type):
					echo "/* " . $filename . " */";
					echo $type . "";
                endforeach;
				echo "</script>";
			endif;
			echo "</head>";
			echo "<body class='".$this->Browser->Name." ".$this->Browser->Version."'>";
		}

        function displaySubHeader($conetnt, $alt = "", $title = "")
        {
            /*
            echo "<div class='subheader container_24'>";
                echo "<span alt='".$alt."' title='".$title."'>".strtoupper($conetnt)."</span>";
            echo "</div>";
            //*/
        }

        function displayHeader()
        {
			echo "<div id='Header'>";
                echo "<div id='Icon' class='container_24'>";
                    echo "<a target='_blank' href='http://www.monash.edu.au'><img class='cursor logo' src='".IMAGE_FOLDER."monash_logo.png'></a>";
                    //echo "<img class='cursor monashlogo' onclick='return imagehref(\"".ROOT_HTTP_SERVER."\");' src='".IMAGE_FOLDER."leadership_logo.png'>";
                    echo "<a href='".ROOT_HTTP_SERVER."'><span class='cursor banner-right'>John Bertrand Leadership Series</span></a>";
                echo"</div>";
                echo "<div id='Menu' class='container_24'>";
                    $this->Boxes->get_mainmenu();
                echo "</div>";
			echo "</div>";
        }

		function displayFooter()
        {
			echo "<div id='Footer'>";
                /*
                echo "<div class='content container_24'>";
                    echo "<div class='left footerbox grid_custom'>";
                        echo "<h2>".strtoupper("Additional Links for participants")."</h2>";
                        echo "<hr class='devider' />";
                        $this->Boxes->get_footermenu();
                    echo "</div>";

                    echo "<div class='middle footerbox grid_custom'>";
                        echo "<h2>".strtoupper("proud partners")."</h2>";
                        echo "<hr class='devider' />";
                        $this->Boxes->get_footerpartner();
                    echo "</div>";

                    echo "<div class='right footerbox grid_custom'>";
                        echo "<h2>".strtoupper("follow")."</h2>";
                        echo "<hr class='devider' />";
                        $this->Boxes->get_footersocial();
                    echo "</div>";
                echo"</div>";
                */
                echo "<div class='producer container_24'><span>Proudly designed and developed by (project partners)</span> <a href='http://www.unijobs.com.au/' target='_blank'><span style='color: #f78f20;'>UniJobs Australia - Australia's University and academic Jobs Website</span> <img src='".IMAGE_FOLDER."unijobs_logo.png' alt='Australia's University and academic Jobs Website' title='Australia's University and academic Jobs Website' /></a></div>";
                echo "<div class='copyright container_24'>Copyright &copy; ".date("Y")." Monash University. ABN 12 377 614 012 Accessibility - Caution - Privacy CRICOS Provider Number: 00008C</div>";
			echo "</div>";
			echo "</body>";
			echo "</html>";
		}
	}
?>