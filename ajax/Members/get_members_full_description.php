<?php
    require_once "../../top.php";
    require_once MEMBERS_INCLUDE_ADDRESS."Members.php";

    if(!isset($_POST['type']) || !isset($_POST['key'])):
        echo "failed";
        exit;
    endif;

    if(!isset($Members[$_POST['key']])):
        echo "failed";
        exit;
    endif;

    if($_POST['type'] == "summary")
        echo $Members[$_POST['key']]['description'];

    if($_POST['type'] == "detailed")
        echo $Members[$_POST['key']]['summary'];

    require_once "../../down.php";
?>