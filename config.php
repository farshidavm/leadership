<?php
  define('DOMAIN', 'gaexec.com/leadership');

  define('ADDRESS', dirname(__FILE__) . '/');

  define('HTTP_SERVER', 'http://'.DOMAIN);
  define('HTTPS_SERVER', 'http://'.DOMAIN);

  define('ROOT_HTTP_SERVER', HTTP_SERVER.'/');
  define('ROOT_HTTPS_SERVER', HTTPS_SERVER.'/');

  //AJAX FOLDER
  define('AJAX_FOLDER', ROOT_HTTP_SERVER.'ajax/');
  define('AJAX_ADDRESS', ADDRESS.'ajax/');

  //CONTENT FOLDER
  define('CONTENT_FOLDER', ROOT_HTTP_SERVER.'content/');
  define('CONTENT_ADDRESS', ADDRESS.'content/');

  //CONTROL FOLDER
  define('CONTROL_FOLDER', ROOT_HTTP_SERVER.'control/');
  define('CONTROL_ADDRESS', ADDRESS.'control/');

  //CSS Folder
  define('CSS_FOLDER', ROOT_HTTP_SERVER.'css/');
  define('CSS_ADDRESS', ADDRESS.'css/');

  //FILE Folder
  define('FILE_FOLDER', ROOT_HTTP_SERVER.'files/');
  define('FILE_ADDRESS', ADDRESS.'files/');

  //IMAGE FOLDER
  define('IMAGE_FOLDER', ROOT_HTTP_SERVER.'images/');
  define('IMAGE_ADDRESS', ADDRESS.'images/');

  define('BOX_FOLDER', IMAGE_FOLDER.'Box/');
  define('BOX_ADDRESS', IMAGE_ADDRESS.'Box/');

  define('CONVERSATION_IMAGE_FOLDER', IMAGE_FOLDER.'Conversation/');
  define('CONVERSATION_IMAGE_ADDRESS', IMAGE_ADDRESS.'Conversation/');

  define('EVENTS_IMAGE_FOLDER', IMAGE_FOLDER.'Events/');
  define('EVENTS_IMAGE_ADDRESS', IMAGE_ADDRESS.'Events/');  

  define('MEMBERS_IMAGE_FOLDER', IMAGE_FOLDER.'Members/');
  define('MEMBERS_IMAGE_ADDRESS', IMAGE_ADDRESS.'Members/');

  define('AVATAR_MEMBERS_IMAGE_FOLDER', MEMBERS_IMAGE_FOLDER.'Avatar/');
  define('AVATAR_MEMBERS_IMAGE_ADDRESS', MEMBERS_IMAGE_ADDRESS.'Avatar/');

  define('PURPOSE_IMAGE_FOLDER', IMAGE_FOLDER.'Purpose/');
  define('PURPOSE_IMAGE_ADDRESS', IMAGE_ADDRESS.'Purpose/');

  define('SPEAKERS_IMAGE_FOLDER', IMAGE_FOLDER.'Speakers/');
  define('SPEAKERS_IMAGE_ADDRESS', IMAGE_ADDRESS.'Speakers/');

  //INCLUDE FOLDER
  define('INCLUDE_FOLDER', ROOT_HTTP_SERVER.'include/');
  define('INCLUDE_ADDRESS', ADDRESS.'include/');

  define('BOX_INCLUDE_FOLDER', INCLUDE_FOLDER.'Box/');
  define('BOX_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Box/');

  define('BROWSER_INCLUDE_FOLDER', INCLUDE_FOLDER.'Browser/');
  define('BROWSER_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Browser/');  

  define('CONVERSATION_INCLUDE_FOLDER', INCLUDE_FOLDER.'Conversation/');
  define('CONVERSATION_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Conversation/');

  define('DATABASE_INCLUDE_FOLDER', INCLUDE_FOLDER.'Database/');
  define('DATABASE_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Database/');

  define('EMAIL_INCLUDE_FOLDER', INCLUDE_FOLDER.'Email/');
  define('EMAIL_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Email/');

  define('FORM_INCLUDE_FOLDER', INCLUDE_FOLDER.'Form/');
  define('FORM_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Form/');

  define('FUNCTION_INCLUDE_FOLDER', INCLUDE_FOLDER.'Function/');
  define('FUNCTION_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Function/');

  define('MEMBERS_INCLUDE_FOLDER', INCLUDE_FOLDER.'Members/');
  define('MEMBERS_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Members/');

  define('PAGE_INCLUDE_FOLDER', INCLUDE_FOLDER.'Page/');
  define('PAGE_INCLUDE_ADDRESS', INCLUDE_ADDRESS.'Page/');

  //JS FOLDER
  define('JS_FOLDER', ROOT_HTTP_SERVER.'js/');
  define('JS_ADDRESS', ADDRESS.'js/');

  //MODEL FOLDER
  define('MODEL_FOLDER', ROOT_HTTP_SERVER.'model/');
  define('MODEL_ADDRESS', ADDRESS.'model/');

  //VIEW FOLDER
  define('VIEW_FOLDER', ROOT_HTTP_SERVER.'view/');
  define('VIEW_ADDRESS', ADDRESS.'view/');

  //SERVER SETTING
  define('ENABLE_SSL', false);
  define('HTTP_COOKIE_DOMAIN', HTTP_SERVER);
  define('HTTPS_COOKIE_DOMAIN', HTTPS_SERVER);
  define('HTTP_COOKIE_PATH', '/');
  define('HTTPS_COOKIE_PATH', '/');

  //Library
  include_once BROWSER_INCLUDE_ADDRESS.'Browser.php';
  include_once BOX_INCLUDE_ADDRESS.'Boxes.php';
  include_once FORM_INCLUDE_ADDRESS.'Check.php';
  include_once CONVERSATION_INCLUDE_ADDRESS.'Conversation.php';
  include_once DATABASE_INCLUDE_ADDRESS.'Database.php';
  include_once FUNCTION_INCLUDE_ADDRESS.'Function.php';
  include_once PAGE_INCLUDE_ADDRESS.'Page.php';
  //Library

  //Members
  include_once MEMBERS_INCLUDE_ADDRESS."Members.php";
  //Members

  //MVC
  include_once CONTROL_ADDRESS."Control.php";
  include_once MODEL_ADDRESS."Model.php";
  include_once VIEW_ADDRESS."View.php";
  //MVC
?>